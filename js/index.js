var app = new Vue({
    el: '#app',
    data: {
        message: '',
        interactionid: '',
        port: 6969,
        protocol: 'http',
        dispositioncode: 'type disposition code here',
        markdone: false,
        checked: true
    },
    methods: {
      releaseCall: async function () {
        var command = {'command':'INTERACTIONRELEASE', 'sessionid':'12345', 'interactionid': (this.checked ? this.interactionid : "")};
        var mycall = `${this.protocol}://localhost:${this.port}/executecommand?message=` + JSON.stringify(command) + "&jsonp_callback=commandResponse";
        this.executeCommand(mycall);
      },
      markDone: async function () {
        var command = {'command':'INTERACTIONMARKDONE', 'sessionid':'12345', 'interactionid': (this.checked ? this.interactionid : "")};
        var mycall = `${this.protocol}://localhost:${this.port}/executecommand?message=` + JSON.stringify(command) + "&jsonp_callback=commandResponse";
        this.executeCommand(mycall);
        /*
        var retVal = await fetch(`${this.protocol}://localhost:${this.port}/executecommand?message=` + JSON.stringify(command),
        {
            mode:'no-cors'
        });
        */
      },
      setDispositionCode: async function () {
        var command = {'command':'SETDISPOSITIONCODE', 'sessionid':'12345', 'interactionid': (this.checked ? this.interactionid : ""), 'markdone': this.markdone, 'dispositioncode': this.dispositioncode };
        var mycall = `${this.protocol}://localhost:${this.port}/executecommand?message=` + JSON.stringify(command) + "&jsonp_callback=commandResponse";
        this.executeCommand(mycall);
      },
      executeCommand: async function (messageCommand) {
        var script = document.createElement('script');
        script.src = messageCommand;
        document.body.appendChild(script);
      },
      filterInteraction: function () {
        console.log("Clicked" + this.checked);
      }
    }
  })
  
  function commandResponse(commandResponseObj)
  {
    console.log("commandResponse: ", commandResponseObj);
  }

  app.interactionid = new URL(location.href).searchParams.get('interactionid');
  app.port = new URL(location.href).searchParams.get('port') || '6969';
  app.protocol = new URL(location.href).searchParams.get('protocol') || 'http';
  app.checked = (app.interactionid ? true : false);
    